package org.helixdev.bukkit.example.command;

import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.helixdev.command.annotaion.Arg;
import org.helixdev.command.annotaion.Command;
import org.helixdev.command.annotaion.Sender;

public class CreateMapCommand {

    @Command(value = "create", description = "", aliases = "c")
    public void createMap(@Sender CommandSender sender, @Arg("name") String name, @Arg(value = "world") World world) {
        sender.sendMessage("Map created in " + world.getName());
    }

}
