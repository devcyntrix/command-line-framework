package org.helixdev.bukkit.example.command;

import org.helixdev.command.annotaion.CommandContainer;
import org.helixdev.command.annotaion.Require;

@CommandContainer("test")
@Require("test.hallo")
public class TestCommand {

    @CommandContainer(value = "map", innerContainers = CreateMapCommand.class)
    public static class Map {

    }

}
