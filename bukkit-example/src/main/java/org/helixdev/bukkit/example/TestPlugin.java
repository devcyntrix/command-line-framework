package org.helixdev.bukkit.example;

import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.TabCompleteEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;
import org.helixdev.bukkit.command.PluginCommandMap;
import org.helixdev.bukkit.example.command.TestCommand;
import org.helixdev.command.annotaion.Arg;
import org.helixdev.command.annotaion.Command;
import org.helixdev.command.annotaion.Require;
import org.helixdev.command.annotaion.Sender;
import org.helixdev.command.annotaion.string.Greedy;
import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parameter.FlagCommandParameter;

public class TestPlugin extends JavaPlugin implements Listener {

    private PluginCommandMap map;

    @SneakyThrows
    @Override
    public void onDisable() {

        if (this.map != null) {
            this.map.close();
        }

    }

    @EventHandler
    public void onTest(TabCompleteEvent event) {
        event.getSender().sendMessage(event.getCompletions().toString());
    }

    @Override
    public void onEnable() {

        this.map = PluginCommandMap.create(this);
//        this.map.register(new Test());
        this.map.register(new TestCommand());
        this.map.register(this);

        getServer().getPluginManager().registerEvents(this, this);

        map.createCommand("hallo test")
                .parameters(
                        new CommandParameter("test", Player.class),
                        new FlagCommandParameter("motion", Vector.class, null, "0 4 0", 'v'),
                        new FlagCommandParameter("test", boolean.class, 'f')
                )
                .consumer(ctx -> {
                    Player test = ctx.getValue("test");
                    System.out.println("Vector: " + ctx.getValue("motion"));
                    test.setVelocity(ctx.getValue("motion"));
                })
                .build();
//
//        System.out.println(map);

//        // check if brigadier is supported
//        if (CommodoreProvider.isSupported()) {
//            System.out.println("SUPPORTED!");
//
//            // get a commodore instance
//            Commodore commodore = CommodoreProvider.getCommodore(this);
//
//            commodore.register(LiteralArgumentBuilder.literal("mycommand")
//                    .then(RequiredArgumentBuilder.argument("some-argument", StringArgumentType.string())
//                            .then(RequiredArgumentBuilder.argument("some-other-argument", BoolArgumentType.bool()))
//                            .executes(commandContext -> {
//                                CommandSender commandSender = commodore.getBukkitSender(commandContext.getSource());
//                                commandSender.sendMessage("Test");
//                                return 0;
//                            }).build()));
//
//        } else {
//            System.out.println("UNSUPPORTED!");
//        }

    }

    @Command("broadcast")
    public void executeBroadcast(@Arg("message") @Greedy String message, int tabs) {
        Bukkit.broadcastMessage("§c" + message);
    }

    @Command(value = "test sound")
    public void onSoundTest(@Sender Player player, Sound material) {
        player.sendMessage("test: " + material);
    }

    @Command(value = "test material")
    @Require("test.test")
    public void onMaterialTest(@Sender Player player, Material material) {
        player.sendMessage("test: " + material);
    }

    @Command(value = "test both")
    public void onTestBoth(@Sender Player player, Material material, Sound sound) {
        player.sendMessage("test: " + material + " " + sound);
    }

    @Command(value = "test vector")
    public void onTestVector(@Sender Player player, Vector vector) {
        player.sendMessage("test: " + vector);
    }

//    @CommandContainer("buildffa")
//    public static class Test {
//
//        @CommandContainer("map")
//        public static class Map {
//
//            @Command(name = "create", description = "test")
//            public void ontest(@Sender CommandSender sender,
//                               @Range(from = 0, to = 10) @Arg(value = "test") Integer a,
//                               @Greedy @Arg(value = "name", def = "Ricardo") String name,
//                               @Flag(value = 't', name = "t", def = "true") boolean t,
//                               @Flag(value = 'c', name = "c") boolean c,
//                               @Quoted @Flag(value = 'h', name = "hallo") String hallo) {
//                sender.sendMessage("Sender: " + sender.getName());
//                sender.sendMessage("test: " + a);
//                sender.sendMessage("name: " + name);
//                sender.sendMessage("Flag 't': " + t);
//                sender.sendMessage("Flag 'c': " + c);
//                sender.sendMessage("Flag 'h': " + hallo);
//            }
//
//
//            @Command(name = "info", description = "test")
//            public void ontesat(@Sender CommandSender sender, @Range(from = 0, to = 10) @Arg(value = "test") Integer a, @Greedy @Arg(value = "name", def = "Ricardo") String name, @Flag(value = 't', def = "true") boolean t, @Flag('c') boolean c, @Quoted @Flag(value = 'h', name = "hallo") String hallo) {
//                sender.sendMessage("Sender: " + sender.getName());
//                sender.sendMessage("test: " + a);
//                sender.sendMessage("name: " + name);
//                sender.sendMessage("Flag 't': " + t);
//                sender.sendMessage("Flag 'c': " + c);
//                sender.sendMessage("Flag 'h': " + hallo);
//            }
//
//        }
//
//        @CommandContainer("test")
//        public static class TestA {
//
//            @Command(name = "create", description = "test")
//            public void ontest(@Sender CommandSender sender, @Arg("target") Player player) {
//                sender
//                        .sendMessage(
//                                player.getUniqueId().toString()
//                        );
//            }
//
//        }
//
//    }
}
