package org.helixdev.bungee.command.test;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.helixdev.command.annotaion.Arg;
import org.helixdev.command.annotaion.Command;
import org.helixdev.command.annotaion.Sender;

public class TestCommand {

    @Command(value = "create", description = "Test command", aliases = "c")
    public void createMap(@Sender CommandSender sender, @Arg(value = "target", description = "Target player") ProxiedPlayer name) {
        sender.sendMessage("Given name " + name.getName());
    }

}
