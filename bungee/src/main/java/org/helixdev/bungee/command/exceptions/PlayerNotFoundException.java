package org.helixdev.bungee.command.exceptions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.helixdev.command.exception.CommandException;

@Getter
@RequiredArgsConstructor
public class PlayerNotFoundException extends CommandException {

    private final String name;

}
