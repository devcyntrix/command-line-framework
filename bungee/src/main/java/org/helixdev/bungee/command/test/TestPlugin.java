package org.helixdev.bungee.command.test;

import net.md_5.bungee.api.plugin.Plugin;
import org.helixdev.bungee.command.PluginCommandMap;

public class TestPlugin extends Plugin {

    @Override
    public void onEnable() {
        PluginCommandMap pluginCommandMap = PluginCommandMap.create(this);

        pluginCommandMap.register(new TestCommand());
    }
}
