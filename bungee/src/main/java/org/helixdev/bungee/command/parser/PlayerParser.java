package org.helixdev.bungee.command.parser;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.helixdev.bungee.command.exceptions.PlayerNotFoundException;
import org.helixdev.command.ArgumentStack;
import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parser.Parser;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PlayerParser implements Parser<ProxiedPlayer> {

    @Override
    public <E> @NotNull ProxiedPlayer parse(E executor, ArgumentStack stack, Class<?> parseTo, CommandParameter parameter) {
        String name = stack.pop();

        if (name.equals("@r")) {
            Collection<? extends ProxiedPlayer> players = ProxyServer.getInstance().getPlayers();
            Optional<? extends ProxiedPlayer> first = players.stream()
                    .skip((int) (players.size() * Math.random()))
                    .findFirst();
            if (!first.isPresent())
                throw new PlayerNotFoundException("Random");
            return first.get();
        }

        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(name);
        if (player == null)
            throw new PlayerNotFoundException(name);
        return player;
    }

    @Override
    public <E> Collection<String> getSuggestionsStrings(E executor, ArgumentStack args, String written, CommandParameter parameter) {
        List<String> collect = ProxyServer.getInstance().getPlayers().stream().filter(player -> player.getName().toLowerCase().startsWith(written.toLowerCase()) || player.getName().toLowerCase().endsWith(written.toLowerCase()))
                .map(ProxiedPlayer::getName)
                .collect(Collectors.toList());
        collect.add("@r");
        return collect;
    }
}
