package org.helixdev.bungee.command;

import com.google.gson.reflect.TypeToken;
import lombok.Getter;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.Connection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;
import org.helixdev.bungee.command.parser.PlayerParser;
import org.helixdev.bungee.command.util.StringUtil;
import org.helixdev.command.ArgumentStack;
import org.helixdev.command.CommandExecutor;
import org.helixdev.command.CommandMap;
import org.helixdev.command.exception.CommandException;
import org.helixdev.command.exception.InvalidFlagException;
import org.helixdev.command.exception.SyntaxException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class PluginCommandMap extends CommandMap<CommandSender> implements Listener {

    @Getter
    private final Plugin plugin;

    private final Map<String, Command> commandCache = new HashMap<>();

    protected PluginCommandMap(@Nullable CommandMap<CommandSender> parent, @NotNull Plugin plugin) {
        super(parent);
        this.plugin = plugin;
        super.setPermittedPredicate(CommandSender::hasPermission);

        getParserProvider().register(new PlayerParser(), new TypeToken<ProxiedPlayer>() {
        }.getType());

        getErrorHandlerRegistry().register(SyntaxException.class, (executor, exception) -> executor.sendMessage("§c/" + exception.getMessage()));
        getErrorHandlerRegistry().register(InvalidFlagException.class, (executor, exception) -> executor.sendMessage("§cInvalid flag: " + ((InvalidFlagException) exception).getFlag()));

        ProxyServer.getInstance().getPluginManager().registerListener(plugin, this);
    }

    public static @NotNull PluginCommandMap create(@NotNull Plugin plugin) {
        return new PluginCommandMap(null, plugin);
    }

    @Override
    public CommandExecutor<CommandSender> registerExecutor(String label, CommandExecutor<CommandSender> executor) {
        CommandExecutor<CommandSender> success = super.registerExecutor(label, executor);

        if (success == null)
            return null;

        String name = label;

        if (name.contains(ARGUMENT_SEPARATOR))
            name = name.split(ARGUMENT_SEPARATOR)[0];

        String finalName = name;

        Command command = new Command(finalName, executor.getPermission(), executor.getAliases().toArray(new String[0])) {
            @Override
            public void execute(CommandSender commandSender, String[] args) {
                System.out.println("Running");

                try {
                    ArgumentStack stack = ArgumentStack.createInvert(args);
                    stack.push(finalName);

                    System.out.println(stack);

                    execute1(commandSender, stack);
                } catch (CommandException e) {
                    boolean handleSuccess = PluginCommandMap.this.handleError(commandSender, e);
                    if (!handleSuccess) {
                        throw e;
                    }
                }
            }
        };

        commandCache.put(name, command);
        success.getAliases().forEach(s -> commandCache.put(s, command));

        ProxyServer.getInstance().getPluginManager().registerCommand(plugin, command);

        return success;
    }

    @EventHandler
    public void onTabComplete(TabCompleteEvent event) {
        Connection connection = event.getSender();

        System.out.println("complete");

        if (!(connection instanceof CommandSender)) {
            return;
        }

        System.out.println("is sender");

        CommandSender commandSender = (CommandSender) connection;
        String[] allArguments = event.getCursor().split(" ");
        String label = allArguments[0].substring(1);
        System.out.println("label = " + label);

        Command command = commandCache.get(label);

        if (command == null) {
            return;
        }

        System.out.println("command = " + command);

        LinkedList<String> args = new LinkedList<>(Arrays.asList(Arrays.copyOfRange(allArguments, 1, allArguments.length)));

        String written = "";
        if (!args.isEmpty()) {
            written = args.removeLast();
        }
        args.addFirst(command.getName());
        Collection<String> suggestions = getSuggestions(commandSender, ArgumentStack.createInvert(args.toArray(new String[0])), written);

        List<String> completions = new LinkedList<>();
        StringUtil.copyPartialMatches(written, suggestions, completions);

        event.getSuggestions().clear();
        event.getSuggestions().addAll(suggestions);
    }

    public void execute1(CommandSender executor, ArgumentStack arguments) {
        execute(executor, arguments);
    }
}
