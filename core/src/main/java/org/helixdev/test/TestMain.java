package org.helixdev.test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestMain {

    private volatile int test = 0;

    public void test() {

        ExecutorService service = Executors.newWorkStealingPool();

        for (int i = 0; i < 1000; i++) {
            service.execute(() -> increase());
        }
        service.shutdown();

        while(!service.isTerminated()) {}

        System.out.println(test);
    }

    public void increase() {
        test++;
    }

    public int get() {
        return test;
    }

    public static void main(String[] args) {
        new TestMain().test();
    }

}
