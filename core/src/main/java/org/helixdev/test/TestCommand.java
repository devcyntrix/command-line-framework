package org.helixdev.test;

import org.helixdev.command.annotaion.*;
import org.helixdev.command.annotaion.string.Greedy;
import org.helixdev.command.annotaion.string.Quoted;

@CommandContainer(value = "test", innerContainers = InnerContainerTest.class)
public class TestCommand {

    // Rooot
//    @Command(name = "", description = "")
//    public void test(@Sender Integer sender, @Flag(value = 'p', def = "1") int page) {
//        System.out.println("MAP: " + page);
//    }

    @Command(value = "create", description = "test")
    public void ontest(@Sender Integer sender, @Range(from = 0, to = 10) @Arg(value = "test") Integer a, @Greedy String name, @Flag(value = 't', def = "true") boolean t, @Flag('c') boolean c, @Quoted @Flag(value = 'h', name = "hallo") String hallo) {
        System.out.println("Sender: " + sender);
        System.out.println("test: " + a);
        System.out.println("name: " + name);
        System.out.println("Flag 't': " + t);
        System.out.println("Flag 'c': " + c);
        System.out.println("Flag 'h': " + hallo);
    }

    @Command(value = "test Map", description = "")
    public void createMap(@Sender Integer sender) {

    }

}
