package org.helixdev.test;

import org.helixdev.command.annotaion.Command;
import org.helixdev.command.annotaion.CommandContainer;

@CommandContainer("test")
public class InnerContainerTest {

    @Command("hallo")
    public void onTest() {
        System.out.println("Hallo");
    }

}
