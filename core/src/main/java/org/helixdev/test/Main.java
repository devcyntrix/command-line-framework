package org.helixdev.test;

import org.helixdev.command.CommandMap;
import org.helixdev.command.ErrorHandler;
import org.helixdev.command.exception.CommandException;
import org.helixdev.command.exception.SyntaxException;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        CommandMap<Integer> map = CommandMap.create();
//        map.createCommand("test")
//                .parameters(new Parameter("test", String.class, new QuotedAttribute()))
//                .consumer(integerExecutionContext -> {
//                    String test = integerExecutionContext.getValue("test");
//                    System.out.println(test);
//                })
//                .build();
        map.register(new TestCommand());
        map.catchException(SyntaxException.class, new ErrorHandler<Integer, CommandException>() {
            @Override
            public void handleError(Integer executor, CommandException exception) {
                System.err.println(exception.getMessage());
            }
        });


//        CommandMap<Object> map = CommandMap.create();
//        map.register(new TestCommand());
//        map.setErrorHandler((executor, exception) -> {
//            if (exception instanceof SyntaxException) {
//                System.err.println("Wrong syntax: " + exception.getMessage());
//                return;
//            }
//            System.err.println(exception.getClass().getCanonicalName());
//            System.err.println(exception.getMessage());
//            exception.printStackTrace();
//        });
//
//
//        System.out.println(map);
//
        Scanner scanner = new Scanner(System.in);

        int i = 0;

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();

            boolean execute = map.execute(i++, line);
            if (!execute) {
                System.out.println("Unknown command");
            }
        }

    }

}
