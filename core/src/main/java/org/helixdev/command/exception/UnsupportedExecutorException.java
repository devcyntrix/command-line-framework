package org.helixdev.command.exception;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UnsupportedExecutorException extends CommandException {

    private final Class<?> executorClass;

}

