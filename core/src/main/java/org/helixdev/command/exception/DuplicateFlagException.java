package org.helixdev.command.exception;

import lombok.Getter;
import org.helixdev.command.annotaion.Flag;
import org.helixdev.command.parameter.FlagCommandParameter;

@Getter
public class DuplicateFlagException extends CommandException {

    private FlagCommandParameter flag;

    private Flag f;

    public DuplicateFlagException(FlagCommandParameter flag) {
        this.flag = flag;
    }

    public DuplicateFlagException(Flag f) {
        this.f = f;
    }
}
