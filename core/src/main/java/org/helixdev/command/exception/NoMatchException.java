package org.helixdev.command.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.helixdev.command.parameter.attribute.string.MatchesAttribute;

@Getter
@AllArgsConstructor
public class NoMatchException extends CommandException {

    private final MatchesAttribute attribute;

}
