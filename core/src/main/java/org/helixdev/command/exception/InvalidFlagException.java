package org.helixdev.command.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class InvalidFlagException extends CommandException {

    private final String flag;

}
