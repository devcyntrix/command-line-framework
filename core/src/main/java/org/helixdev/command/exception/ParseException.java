package org.helixdev.command.exception;

import lombok.Getter;
import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parser.Parser;

@Getter
public class ParseException extends CommandException {

    private final Parser<?> parser;
    private final CommandParameter parameter;

    public ParseException(Parser<?> parser, CommandParameter parameter) {
        this.parser = parser;
        this.parameter = parameter;
    }

    public ParseException(String message, Parser<?> parser, CommandParameter parameter) {
        super(message);
        this.parser = parser;
        this.parameter = parameter;
    }

    public ParseException(String message, Throwable cause, Parser<?> parser, CommandParameter parameter) {
        super(message, cause);
        this.parser = parser;
        this.parameter = parameter;
    }

    public ParseException(Throwable cause, Parser<?> parser, CommandParameter parameter) {
        super(cause);
        this.parser = parser;
        this.parameter = parameter;
    }
}
