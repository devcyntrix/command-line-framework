package org.helixdev.command.exception;

import lombok.Getter;

@Getter
public class ToManyArgumentsException extends CommandException {

    private final int registeredCount;
    private final String[] arguments;

    public ToManyArgumentsException(int registeredCount, String[] arguments) {
        super(String.join(" ", arguments));
        this.registeredCount = registeredCount;
        this.arguments = arguments;
    }
}
