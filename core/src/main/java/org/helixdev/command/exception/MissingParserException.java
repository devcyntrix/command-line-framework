package org.helixdev.command.exception;

public class MissingParserException extends CommandException {

    public MissingParserException() {
    }

    public MissingParserException(String message) {
        super(message);
    }
}
