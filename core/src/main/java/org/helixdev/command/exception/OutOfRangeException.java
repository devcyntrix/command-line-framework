package org.helixdev.command.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.helixdev.command.parameter.attribute.RangeAttribute;

@RequiredArgsConstructor
public class OutOfRangeException extends CommandException {

    @Getter
    private final RangeAttribute rangeAttribute;

}
