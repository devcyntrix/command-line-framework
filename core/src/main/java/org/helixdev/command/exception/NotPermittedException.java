package org.helixdev.command.exception;

import lombok.Getter;

public class NotPermittedException extends CommandException {

    @Getter
    private final String permission;

    public NotPermittedException(String permission) {
        this.permission = permission;
    }
}
