package org.helixdev.command.exception;

public class InvalidParameterAttribute extends CommandException {

    public InvalidParameterAttribute() {
    }

    public InvalidParameterAttribute(String message) {
        super(message);
    }

    public InvalidParameterAttribute(String message, Throwable cause) {
        super(message, cause);
    }
}
