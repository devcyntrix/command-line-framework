package org.helixdev.command.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.helixdev.command.parameter.CommandParameter;

@RequiredArgsConstructor
public class MissingArgumentException extends CommandException {

    @Getter
    private final CommandParameter parameter;

}
