package org.helixdev.command.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.helixdev.command.parameter.FlagCommandParameter;

@RequiredArgsConstructor
public class MissingFlagArgument extends CommandException {

    @Getter
    private final FlagCommandParameter flagParameter;

}
