package org.helixdev.command.exception;

public class InvalidQuoteException extends CommandException {

    public InvalidQuoteException(String message) {
        super(message);
    }
}
