package org.helixdev.command.exception;

public class SyntaxException extends CommandException {

    public SyntaxException() {
    }

    public SyntaxException(String message) {
        super(message);
    }
}
