package org.helixdev.command.parameter.attribute;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.helixdev.command.exception.InvalidParameterAttribute;
import org.helixdev.command.parameter.CommandParameter;

import java.math.BigDecimal;
import java.math.BigInteger;

@Getter
@AllArgsConstructor
public class RangeAttribute extends ParameterAttribute {

    private final double from;
    private final double to;

    @Override
    public void validate(CommandParameter parameter) {
        if (!Number.class.isAssignableFrom(parameter.getType())) {
            throw new InvalidParameterAttribute("The range attribute can only use with the types which are assignable from type \"number\"");
        }
        if (BigInteger.class.isAssignableFrom(parameter.getType()) || BigDecimal.class.isAssignableFrom(parameter.getType())) {
            throw new InvalidParameterAttribute("the range attribute doesn't support big integer or big decimal numbers.");
        }
    }
}
