package org.helixdev.command.parameter.attribute.string;

import org.helixdev.command.exception.InvalidParameterAttribute;
import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parameter.FlagCommandParameter;

public class GreedyAttribute extends StringAttribute {

    @Override
    public void validate(CommandParameter parameter) {
        if (parameter instanceof FlagCommandParameter) {
            throw new InvalidParameterAttribute("The greedy attribute cannot use with flag");
        }
        super.validate(parameter);
    }
}
