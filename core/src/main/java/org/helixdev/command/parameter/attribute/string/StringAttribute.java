package org.helixdev.command.parameter.attribute.string;

import org.helixdev.command.exception.InvalidParameterAttribute;
import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parameter.attribute.ParameterAttribute;

public abstract class StringAttribute extends ParameterAttribute {

    @Override
    public void validate(CommandParameter parameter) {
        if (parameter.getType() != String.class) {
            throw new InvalidParameterAttribute("The greedy attribute can only use with the type \"string\"");
        }
    }
}
