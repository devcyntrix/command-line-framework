package org.helixdev.command.parameter;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.helixdev.command.Argument;
import org.helixdev.command.parameter.attribute.AttributeMap;
import org.helixdev.command.parameter.attribute.ParameterAttribute;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

@Data
@EqualsAndHashCode
public class CommandParameter {

    @NotNull
    private final String name;
    @NotNull
    private final Class<?> type;
    @Nullable
    private String description;
    @Nullable
    private String defaultValue;

    private AttributeMap attributeMap = new AttributeMap();

    public CommandParameter(@NotNull String name, @NotNull Class<?> type, @NotNull ParameterAttribute... attributes) {
        this.name = name;
        this.type = type;

        for (ParameterAttribute attribute : attributes) {
            this.attributeMap.put(attribute.getClass(), attribute);
        }
    }

    public CommandParameter(@NotNull String name, @NotNull Class<?> type, @Nullable String description, @Nullable String defaultValue, @NotNull ParameterAttribute... attributes) {
        this.name = name;
        this.type = type;
        this.description = description;
        this.defaultValue = defaultValue;

        for (ParameterAttribute attribute : attributes) {
            attribute.validate(this);
            this.attributeMap.put(attribute.getClass(), attribute);
        }
    }

    public boolean hasAttribute(Class<? extends ParameterAttribute> type) {
        return this.attributeMap.containsKey(type);
    }

    public <T extends ParameterAttribute> @Nullable T getAttribute(Class<T> type) {
        return (T) this.attributeMap.get(type);
    }

    public Argument newArgument(@NotNull Object value) {
        return new Argument(this, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommandParameter parameter = (CommandParameter) o;
        return name.equals(parameter.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Parameter{" +
                "name='" + name + '\'' +
                ", type=" + type +
                '}';
    }
}
