package org.helixdev.command.parameter.attribute.string;

import lombok.Getter;

public class MatchesAttribute extends StringAttribute {

    @Getter
    private final String regexp;

    public MatchesAttribute(String regexp) {
        this.regexp = regexp;
    }
}
