package org.helixdev.command.parameter.attribute;

import org.helixdev.command.parameter.CommandParameter;

public abstract class ParameterAttribute {

    public abstract void validate(CommandParameter parameter);

}
