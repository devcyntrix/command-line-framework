package org.helixdev.command.parameter;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.helixdev.command.parameter.attribute.ParameterAttribute;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@EqualsAndHashCode(callSuper = true)
public class FlagCommandParameter extends CommandParameter {

    private final char flagChar;

    public FlagCommandParameter(@NotNull String name, @NotNull Class<?> type, char flagChar, @NotNull ParameterAttribute... attributes) {
        super(name, type, attributes);
        this.flagChar = flagChar;
    }

    public FlagCommandParameter(@NotNull String name, @NotNull Class<?> type, @Nullable String description, @Nullable String defaultValue, char flagChar, @NotNull ParameterAttribute... attributes) {
        super(name, type, description, defaultValue, attributes);
        this.flagChar = flagChar;
    }

    public boolean isSwitch() {
        return getType() == Boolean.class || getType() == Boolean.TYPE;
    }
}
