package org.helixdev.command;

import com.google.common.base.Defaults;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import org.helixdev.command.annotaion.*;
import org.helixdev.command.annotaion.string.Greedy;
import org.helixdev.command.annotaion.string.Matches;
import org.helixdev.command.annotaion.string.Quoted;
import org.helixdev.command.exception.CommandException;
import org.helixdev.command.exception.SyntaxException;
import org.helixdev.command.exception.UnsupportedExecutorException;
import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parameter.FlagCommandParameter;
import org.helixdev.command.parameter.attribute.ParameterAttribute;
import org.helixdev.command.parameter.attribute.RangeAttribute;
import org.helixdev.command.parameter.attribute.string.GreedyAttribute;
import org.helixdev.command.parameter.attribute.string.MatchesAttribute;
import org.helixdev.command.parameter.attribute.string.QuotedAttribute;
import org.helixdev.command.parser.Parser;
import org.helixdev.command.parser.ParserProvider;
import org.helixdev.command.simple.SimpleCommandExecutorBuilder;
import org.helixdev.command.usage.UsageMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;

import java.io.IOException;
import java.lang.reflect.*;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Consumer;

@Getter
@Setter
public class CommandMap<E> implements CommandExecutor<E> {

    public static final String ARGUMENT_SEPARATOR = " ";

    private final CommandMap<E> parent;
    private final Map<String, CommandExecutor<E>> knownCommands = new HashMap<>();
    private final List<Object> dependencies = new ArrayList<>();

    @Nullable
    private final ParserProvider parserProvider;
    @Nullable
    private final UsageMapper usageMapper;

    private BiPredicate<@NotNull E, @NotNull String> permittedPredicate;

    @Nullable
    private final ErrorHandlerRegistry<E> errorHandlerRegistry;

    @Nullable
    private CommandExecutor<E> rootExecutor;

    @Nullable
    private String permission;

    protected CommandMap(CommandMap<E> parent) {
        this.parent = parent;

        if (parent == null) {
            this.parserProvider = new ParserProvider();
            this.usageMapper = new UsageMapper();
            this.errorHandlerRegistry = new ErrorHandlerRegistry<>();
            this.permittedPredicate = (executor, permission) -> true;
        } else {
            this.parserProvider = null;
            this.usageMapper = null;
            this.errorHandlerRegistry = null;
        }
    }

    public CommandExecutor<E> registerExecutor(String command, CommandExecutor<E> executor) {
        CommandMap<E> map = this;

        if (command.contains(ARGUMENT_SEPARATOR)) {
            String[] split = command.split(ARGUMENT_SEPARATOR);
            map = getMap(
                    Arrays.copyOfRange(split, 0, split.length - 1),
                    true);
            command = split[split.length - 1];
        }

        if (map.knownCommands.put(command.toLowerCase(), executor) != null)
            return null;

        return executor;
    }

    public SimpleCommandExecutorBuilder<E> createCommand(String command) {
        return new SimpleCommandExecutorBuilder<>(command, this);
    }

    public void bindDependency(@NotNull Object dependency) {
        this.dependencies.add(dependency);
    }

    public boolean register(@NotNull Object handler) {
        return register(new String[0], handler);
    }

    public boolean register(@Nullable String[] path, @NotNull Object handler) {
        if (path == null)
            path = new String[0];

        CommandMap<E> map = this;

        Class<?> handlerClass = handler.getClass();
        List<String> pathList = new LinkedList<>(Arrays.asList(path));

        if (handlerClass.isAnnotationPresent(CommandContainer.class)) { // Checks the class annotation
            CommandContainer container = handlerClass.getAnnotation(CommandContainer.class); // Provides the class annotation
            pathList.addAll(Arrays.asList(container.value().split(ARGUMENT_SEPARATOR)));
            map = getMap(pathList.toArray(new String[0]), true, cmap -> {
                if (handlerClass.isAnnotationPresent(Require.class)) {
                    Require require = handlerClass.getAnnotation(Require.class);
                    cmap.setPermission(require.value());
                }
            });


            Class<?>[] classes = container.innerContainers(); // Registers container inner classes
            for (Class<?> nextClass : classes) {
                try {
                    Constructor<?> declaredConstructor = nextClass.getDeclaredConstructor();
                    declaredConstructor.setAccessible(true);
                    map.register(declaredConstructor.newInstance());
                } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

        // Registers inner classes
        Class<?>[] declaredClasses = handlerClass.getDeclaredClasses();
        for (Class<?> declaredClass : declaredClasses) {
            try {
                Constructor<?> declaredConstructor = declaredClass.getConstructor();
                declaredConstructor.setAccessible(true);

                map.register(declaredConstructor.newInstance());
            } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        // Setups dependency fields
        try {
            Field[] declaredFields = handlerClass.getDeclaredFields();
            for (Field declaredField : declaredFields) {
                declaredField.setAccessible(true);
                if (!declaredField.isAnnotationPresent(Dependency.class))
                    continue;
                Object dependency = this.dependencies.stream()
                        .filter(o -> o.getClass() == declaredField.getType())
                        .findFirst()
                        .orElse(null);
                if (dependency == null && declaredField.get(handler) == null)
                    dependency = Defaults.defaultValue(declaredField.getType());
                declaredField.set(handler, dependency);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        // Register commands
        Method[] declaredMethods = handlerClass.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            if (!declaredMethod.isAnnotationPresent(Command.class))
                continue;
            map.registerCommand(handler, declaredMethod);
        }
        return true;
    }

    public CommandMap<E> getMap(String[] path, boolean createIfNoExists) {
        return getMap(path, createIfNoExists, eCommandMap -> {});
    }

    public CommandMap<E> getMap(String[] path, boolean createIfNotExists, Consumer<CommandMap<E>> setup) {
        CommandMap<E> map = this;
        for (String node : path) {
            CommandMap<E> result = map.getMap(node);
            if (result == null && !createIfNotExists) {
                return null;
            }
            if (result == null) {
                result = new CommandMap<>(map);
                setup.accept(result);
                map.registerExecutor(node, result);
            }
            map = result;
        }
        return map;
    }

    public CommandExecutor<E> getCommandExecutor(String name) {
        CommandExecutor<E> ce = getKnownCommands().get(name);
        if (ce != null)
            return ce;
        return getKnownCommands().values()
                .stream()
                .filter(exe -> exe.getAliases().contains(name.toLowerCase()))
                .findFirst()
                .orElse(null); // Check aliases
    }

    public CommandExecutor<E> getCommandExecutor(String[] path) {
        CommandExecutor<E> executor = this;
        for (String node : path) {
            if (executor == null)
                return null;
            if (executor instanceof CommandMap) {
                executor = ((CommandMap) executor).getCommandExecutor(node);
            }
        }
        return executor;
    }

    public CommandExecutor<E> registerCommand(@NotNull Object instance, @NotNull Method method) {
            try {
                method.setAccessible(true);
            } catch (Exception e) {
                return null;
            }

        // Check annotation
        Preconditions.checkArgument(method.isAnnotationPresent(Command.class), "Missing command annotation");

        CommandParameter[] parameters = Arrays.stream(method.getParameters())
                .filter(parameter -> !parameter.isAnnotationPresent(Sender.class))
                .map(parameter -> {
                    String name;
                    String description;
                    String def;

                    if (parameter.isAnnotationPresent(Flag.class)) {
                        Flag flag = parameter.getAnnotation(Flag.class);
                        name = flag.name();
                        description = flag.description();
                        def = flag.def();
                        if (name.isEmpty())
                            name = parameter.getName();
                        if (description.isEmpty())
                            description = null;
                        if (def.isEmpty())
                            def = null;

                        List<ParameterAttribute> list = new LinkedList<>();
                        if (parameter.isAnnotationPresent(Quoted.class)) {
                            list.add(new QuotedAttribute());
                        }
                        if (parameter.isAnnotationPresent(Range.class)) {
                            Range range = parameter.getAnnotation(Range.class);
                            list.add(new RangeAttribute(range.from(), range.to()));
                        }

                        return new FlagCommandParameter(name, parameter.getType(), description, def, flag.value(), list.toArray(new ParameterAttribute[0]));
                    }
                    if (parameter.isAnnotationPresent(Arg.class)) {
                        Arg arg = parameter.getAnnotation(Arg.class);
                        name = arg.value();
                        description = arg.description();
                        def = arg.def();
                        if (name.isEmpty())
                            name = parameter.getName();
                        if (description.isEmpty())
                            description = null;
                        if (def.isEmpty())
                            def = null;
                        // TODO: Parameter attributes
                        List<ParameterAttribute> list = new LinkedList<>();
                        if (parameter.isAnnotationPresent(Quoted.class)) {
                            list.add(new QuotedAttribute());
                        }
                        if (parameter.isAnnotationPresent(Greedy.class)) {
                            list.add(new GreedyAttribute());
                        }
                        if (parameter.isAnnotationPresent(Matches.class)) {
                            Matches matches = parameter.getAnnotation(Matches.class);
                            list.add(new MatchesAttribute(matches.value()));
                        }

                        if (parameter.isAnnotationPresent(Range.class)) {
                            Range range = parameter.getAnnotation(Range.class);
                            list.add(new RangeAttribute(range.from(), range.to()));
                        }
                        return new CommandParameter(name, parameter.getType(), description, def, list.toArray(new ParameterAttribute[0]));
                    }

                    return new CommandParameter(parameter.getName(), parameter.getType());
                }).toArray(CommandParameter[]::new);

        Command command = method.getAnnotation(Command.class);
        String commandName = command.value().toLowerCase();
        String description = command.description();
        if (description.isEmpty())
            description = null;

        List<String> aliases = Arrays
                .stream(command.aliases())
                .map(String::toLowerCase)
                .collect(ImmutableList.toImmutableList());
        if (aliases.isEmpty())
            aliases = Collections.emptyList();

        Require annotation = method.getAnnotation(Require.class);
        if (annotation == null) {
            annotation = instance.getClass().getAnnotation(Require.class);
        }

        // TODO: Find a solution for this problem
//
//        if (commandName.equals("")) {
//            return this.rootExecutor = new SimpleCommandExecutor<>(this, new Consumer<ExecutionContext<E>>() {
//                @Override
//                public void accept(ExecutionContext<E> ctx) {
//
//                }
//            }, description, aliases, annotation.value(), parameters);
//        }

        return createCommand(commandName)
                .parameters(parameters)
                .description(description)
                .aliases(aliases.toArray(new String[0]))
                .permission(annotation != null ? annotation.value() : null)
                .consumer(ctx -> {

                    java.lang.reflect.Parameter[] methodParams = method.getParameters();
                    Object[] values = new Object[methodParams.length];

                    for (int i = 0; i < methodParams.length; i++) {
                        java.lang.reflect.Parameter param = methodParams[i];

                        // Prepare the executor
                        if (param.isAnnotationPresent(Sender.class)) {
                            if (!param.getType().isAssignableFrom(ctx.getExecutor().getClass())) {
                                throw new UnsupportedExecutorException(param.getType());
                            }
                            // Check executor
                            values[i] = ctx.getExecutor();
                            continue;
                        }

                        if (param.isAnnotationPresent(Flag.class)) {
                            Flag f = param.getAnnotation(Flag.class);
                            Object input = ctx.getValue(f.name());

                            Object value = (input == null ?
                                    Defaults.defaultValue(param.getType()) : // No provided flag value
                                    input);

                            values[i] = value;
                            continue;
                        }

                        String name;
                        if (param.isAnnotationPresent(Arg.class)) {
                            name = param.getAnnotation(Arg.class).value();
                        } else {
                            name = param.getName();
                        }

                        Object v = ctx.getValue(name);
                        if (v == null) {
                            v = Defaults.defaultValue(param.getType());
                        }

                        values[i] = v;
                    }

                    // Invoke method
                    try {
                        method.invoke(instance, values);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                })
                .build();
    }

    public CommandMap<E> getMap(String name) {
        CommandExecutor<E> executor = getCommandExecutor(name);
        if (executor != null && !(executor instanceof CommandMap))
            throw new IllegalStateException("Cannot get a command as a map");
        return (CommandMap<E>) executor;
    }

    @Override
    public @NotNull Collection<String> getSuggestions(E executor, ArgumentStack args, String written) {
        if (args.empty()) {
            return Maps.filterEntries(getKnownCommands(), entry -> {
                if (entry.getValue().getPermission() != null) {
                    return getPermittedPredicate().test(executor, entry.getValue().getPermission());
                }
                return true;
            }).keySet();
        }

        CommandExecutor<E> handler = getCommandExecutor(args.pop());
        if (handler == null) {
            return Collections.emptyList();
        }

        return handler.getSuggestions(executor, args, written);
    }


    public boolean execute(E executor, String command) throws CommandException {
        return execute(executor, ArgumentStack.createInvert(command.split(ARGUMENT_SEPARATOR)));
    }

    @Override
    public boolean execute(E executor, ArgumentStack arguments) throws CommandException {
        try {
            if (arguments.empty()) {
                if (this.rootExecutor == null)
                    return false;
                return this.rootExecutor.execute(executor, arguments);
            }

            String command = arguments.pop();
            CommandExecutor<E> exec = getCommandExecutor(command.toLowerCase());
            if (exec == null) {
                if (this.rootExecutor != null) {
                    return this.rootExecutor.execute(executor, arguments);
                }
                return false;
            }

            try {
                return exec.execute(executor, arguments);
            } catch (CommandException e) {
                if (e instanceof SyntaxException) {
                    String syntax = String.join(ARGUMENT_SEPARATOR, command, e.getMessage());
                    SyntaxException exception = new SyntaxException(syntax);
                    if (!handleError(executor, exception))
                        throw exception;
                    return true;
                }
                throw e;
            }
        } catch (CommandException e) {
            if (!handleError(executor, e))
                throw e;
        }
        return true;
    }

    public boolean handleError(E executor, CommandException exception) {
        ErrorHandlerRegistry<E> registry = getErrorHandlerRegistry();
        if (this.parent == null) {
            registry.handleError(executor, exception);
            return true;
        }
        return false;
    }

    /**
     * @see ErrorHandlerRegistry#register(Class, ErrorHandler)
     */
    public void catchException(Class<? extends CommandException> exceptionClass, ErrorHandler<E, CommandException> handler) {
        getErrorHandlerRegistry().register(exceptionClass, handler);
    }

    /**
     * @see ParserProvider#register(Parser, Type, Type...)
     */
    public void registerParser(Parser<?> parser, Type type, Type... types) {
        getParserProvider().register(parser, type, types);
    }

    @Override
    public @Nullable String getDescription() {
        return this.rootExecutor != null ? this.rootExecutor.getDescription() : null;
    }

    @Override
    public @Unmodifiable @NotNull List<String> getAliases() {
        return this.rootExecutor != null ? this.rootExecutor.getAliases() : Collections.emptyList();
    }

    public @NotNull ParserProvider getParserProvider() {
        return this.parent != null ? this.parent.getParserProvider() : Objects.requireNonNull(this.parserProvider);
    }

    public @NotNull UsageMapper getUsageMapper() {
        return this.parent != null ? this.parent.getUsageMapper() : Objects.requireNonNull(this.usageMapper);
    }

    public @NotNull BiPredicate<@NotNull E, @NotNull String> getPermittedPredicate() {
        return this.parent != null ? this.parent.getPermittedPredicate() : this.permittedPredicate;
    }

    public @NotNull ErrorHandlerRegistry<E> getErrorHandlerRegistry() {
        return this.parent != null ? this.parent.getErrorHandlerRegistry() : Objects.requireNonNull(this.errorHandlerRegistry);
    }

    @Override
    public void close() {
        this.knownCommands.values().forEach(executor -> {
            try {
                executor.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        this.knownCommands.clear();
    }

    @Override
    public String toString() {
        return "CommandMap{" +
                "knownCommands=" + knownCommands +
                '}';
    }

    public static <E> @NotNull CommandMap<E> create() {
        return new CommandMap<>(null);
    }

}
