package org.helixdev.command.usage;

import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parser.Parser;
import org.helixdev.command.parser.ParserProvider;
import org.helixdev.command.usage.adapters.GreedyAdapter;
import org.helixdev.command.usage.adapters.NecessityAdapter;
import org.helixdev.command.usage.adapters.RangeAdapter;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class UsageMapper {

    private final List<UsageAdapter> adapters = Arrays.asList(
            new RangeAdapter(),
            new GreedyAdapter(),
            new NecessityAdapter()
    );

    public String map(ParserProvider provider, CommandParameter parameter) {
        Parser<?> parser = provider.getParser(parameter.getType());
        if (parser == null)
            return null;

        List<String> usageList = new LinkedList<>();

        for (CommandParameter argumentName : parser.getParameters(parameter)) {
            String usage = argumentName.getName();
            for (UsageAdapter adapter : adapters) {
                usage = adapter.handle(argumentName, usage);
            }
            usageList.add(usage);
        }

        return String.join(" ", usageList);
    }


}
