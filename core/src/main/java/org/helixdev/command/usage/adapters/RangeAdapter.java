package org.helixdev.command.usage.adapters;

import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parameter.attribute.RangeAttribute;
import org.helixdev.command.usage.UsageAdapter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class RangeAdapter implements UsageAdapter {

    private static final DecimalFormat FORMAT = new DecimalFormat("0.##", DecimalFormatSymbols.getInstance(Locale.GERMAN));

    @Override
    public String handle(CommandParameter parameter, String usage) {
        RangeAttribute attribute = parameter.getAttribute(RangeAttribute.class);
        if (attribute == null)
            return usage;

        double from = attribute.getFrom();
        String fromString = String.valueOf(attribute.getFrom());
        if (from % 1.0 == 0) {
            fromString = String.valueOf((long) from);
        }

        double to = attribute.getTo();
        String toString = String.valueOf(to);
        if (to % 1.0 == 0) {
            toString = String.valueOf((long) to);
        }

        return usage + String.format("(%s-%s)", fromString, toString);
    }
}
