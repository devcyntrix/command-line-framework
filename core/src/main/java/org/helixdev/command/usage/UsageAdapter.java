package org.helixdev.command.usage;

import org.helixdev.command.parameter.CommandParameter;

public interface UsageAdapter {

    String handle(CommandParameter parameter, String usage);

}
