package org.helixdev.command.usage.adapters;

import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parameter.attribute.string.GreedyAttribute;
import org.helixdev.command.usage.UsageAdapter;

public class GreedyAdapter implements UsageAdapter {

    @Override
    public String handle(CommandParameter parameter, String usage) {
        if (!parameter.hasAttribute(GreedyAttribute.class))
            return usage;
        return usage + "...";
    }
}
