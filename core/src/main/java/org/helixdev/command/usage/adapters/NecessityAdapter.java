package org.helixdev.command.usage.adapters;

import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.usage.UsageAdapter;

public class NecessityAdapter implements UsageAdapter {

    @Override
    public String handle(CommandParameter parameter, String usage) {
        if (parameter.getDefaultValue() != null) {
            return "[<" + usage + ">]";
        }
        return "<" + usage + ">";
    }
}
