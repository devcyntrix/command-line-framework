package org.helixdev.command;

import com.google.common.collect.ImmutableMap;
import org.helixdev.command.parameter.CommandParameter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

public class ArgumentMap {

    private final Map<String, Argument> argumentMap;

    public ArgumentMap(@NotNull Argument... arguments) {
        Map<String, Argument> map = new HashMap<>();
        for (Argument argument : arguments) {
            map.put(argument.getParameter().getName().toLowerCase(), argument);
        }
        this.argumentMap = ImmutableMap.copyOf(map);
    }

    public @Nullable Argument getArgument(@NotNull String name) {
        return this.argumentMap.get(name.toLowerCase());
    }

    public boolean contains(@NotNull CommandParameter parameter) {
        return this.argumentMap.values().stream().anyMatch(argument -> argument.getParameter().equals(parameter));
    }

    public boolean contains(@NotNull String name) {
        return this.argumentMap.containsKey(name);
    }
}
