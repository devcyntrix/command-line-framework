package org.helixdev.command;

public interface ErrorHandler<E, EXCEPTION> {

    void handleError(E executor, EXCEPTION exception);

}
