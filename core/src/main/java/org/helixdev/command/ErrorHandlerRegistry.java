package org.helixdev.command;

import lombok.Getter;
import lombok.Setter;
import org.helixdev.command.exception.CommandException;

import java.util.HashMap;
import java.util.Map;

public class ErrorHandlerRegistry<E> {

    @Getter
    @Setter
    private ErrorHandler<E, CommandException> fallbackHandler = (executor, e) -> { throw e; };
    private final Map<Class<? extends CommandException>, ErrorHandler<E, CommandException>> handlerMap = new HashMap<>();

    public <T extends CommandException> ErrorHandler<E, ? extends CommandException> register(Class<T> exceptionClass, ErrorHandler<E, CommandException> handler) {
        return this.handlerMap.put(exceptionClass, handler);
    }

    public ErrorHandler<E, CommandException> getErrorHandler(Class<? extends CommandException> exceptionClass) {
        return this.handlerMap.getOrDefault(exceptionClass, this.fallbackHandler);
    }

    public void handleError(E executor, CommandException exception) {
        ErrorHandler<E, CommandException> errorHandler = getErrorHandler(exception.getClass());
        errorHandler.handleError(executor, exception);
    }

    public ErrorHandler<E, ? extends CommandException> unregister(Class<? extends CommandException> exceptionClass) {
        return this.handlerMap.remove(exceptionClass);
    }

}
