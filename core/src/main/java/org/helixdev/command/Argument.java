package org.helixdev.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.helixdev.command.parameter.CommandParameter;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

@Data
@AllArgsConstructor
public class Argument {

    @NotNull
    private final CommandParameter parameter;
    @NotNull
    private Object value;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Argument argument = (Argument) o;
        return parameter.equals(argument.parameter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parameter);
    }

    @Override
    public String toString() {
        return "Argument{" +
                "parameter=" + parameter +
                ", value=" + value +
                '}';
    }
}
