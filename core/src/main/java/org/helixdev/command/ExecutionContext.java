package org.helixdev.command;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;

@Getter
public class ExecutionContext<E> {

    private final E executor;

    @Unmodifiable
    public final ArgumentMap arguments;

    public ExecutionContext(@NotNull E executor, @NotNull Argument... arguments) {
        this.executor = executor;
        this.arguments = new ArgumentMap(arguments);
    }

    public @Nullable Argument getArgument(@NotNull String name) {
        return this.arguments.getArgument(name);
    }

    public <V> @Nullable V getValue(@NotNull String name) {
        Argument argument = getArgument(name);
        if (argument == null)
            return null;
        return (V) argument.getValue();
    }

}
