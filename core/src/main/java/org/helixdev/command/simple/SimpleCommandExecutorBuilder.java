package org.helixdev.command.simple;

import org.helixdev.command.CommandExecutor;
import org.helixdev.command.CommandMap;
import org.helixdev.command.ExecutionContext;
import org.helixdev.command.parameter.CommandParameter;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Consumer;

public class SimpleCommandExecutorBuilder<E> {

    private final String command;
    private final CommandMap<E> map;

    private @NotNull CommandParameter[] parameters;
    private Consumer<ExecutionContext<E>> consumer;

    private String description;
    private Collection<String> aliases;
    private String permission;

    public SimpleCommandExecutorBuilder(String command, CommandMap<E> map) {
        this.command = command;
        this.map = map;
    }

    public CommandParameter[] parameters() {
        return parameters;
    }

    public SimpleCommandExecutorBuilder<E> parameters(CommandParameter... parameters) {
        this.parameters = parameters;
        return this;
    }

    public Consumer<ExecutionContext<E>> consumer() {
        return consumer;
    }

    public SimpleCommandExecutorBuilder<E> consumer(Consumer<ExecutionContext<E>> consumer) {
        this.consumer = consumer;
        return this;
    }

    public String description() {
        return description;
    }

    public SimpleCommandExecutorBuilder<E> description(String description) {
        this.description = description;
        return this;
    }

    public Collection<String> aliases() {
        return aliases;
    }

    public SimpleCommandExecutorBuilder<E> aliases(String... aliases) {
        this.aliases = Arrays.asList(aliases);
        return this;
    }

    public String permission() {
        return permission;
    }

    public SimpleCommandExecutorBuilder<E> permission(String permission) {
        this.permission = permission;
        return this;
    }

    public CommandExecutor<E> build() {
        return this.map.registerExecutor(this.command, new SimpleCommandExecutor<>(this.map, this.consumer, this.description, this.aliases, this.permission, this.parameters));
    }
}
