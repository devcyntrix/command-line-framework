package org.helixdev.command.simple;

import com.google.common.collect.ImmutableList;
import lombok.Getter;
import org.helixdev.command.*;
import org.helixdev.command.exception.*;
import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parameter.FlagCommandParameter;
import org.helixdev.command.parser.Parser;
import org.helixdev.command.parser.ParserProvider;
import org.helixdev.command.usage.UsageMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;

import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class SimpleCommandExecutor<E> implements CommandExecutor<E> {

    private final CommandMap<E> map;

    @Getter
    private String usage;

    @Getter
    @Nullable
    private final String description;

    @Getter
    @Unmodifiable
    private final List<String> aliases;

    @Getter
    private final String permission;

    @Unmodifiable
    private final List<CommandParameter> parameters;
    @Unmodifiable
    private final List<FlagCommandParameter> flags;
    private final Consumer<ExecutionContext<E>> executionContextConsumer;


    public SimpleCommandExecutor(CommandMap<E> map, Consumer<ExecutionContext<E>> executionContextConsumer, CommandParameter... parameters) {
        this(map, executionContextConsumer, null, null, null, parameters);
    }

    public SimpleCommandExecutor(@NotNull CommandMap<E> map,
                                 @NotNull Consumer<ExecutionContext<E>> executionContextConsumer,
                                 @Nullable String description,
                                 @Nullable Collection<String> aliases,
                                 @Nullable String permission,
                                 @NotNull CommandParameter... parameters) {
        this.map = map;
        this.executionContextConsumer = executionContextConsumer;

        // Separate the array
        List<CommandParameter> parameterList = new LinkedList<>();
        List<FlagCommandParameter> flagList = new LinkedList<>();
        for (CommandParameter parameter : parameters) {
            if (parameter instanceof FlagCommandParameter) {
                flagList.add((FlagCommandParameter) parameter);
                continue;
            }
            parameterList.add(parameter);
        }

        this.parameters = ImmutableList.copyOf(parameterList);
        this.flags = ImmutableList.copyOf(flagList);

        this.usage = generateCommandUsage();
        this.description = description;

        // Setting up aliases
        if (aliases == null) {
            this.aliases = ImmutableList.of();
        } else {
            this.aliases = ImmutableList.copyOf(aliases);
        }

        this.permission = permission;
    }

    @Override
    public boolean execute(E executor, ArgumentStack arguments) {

        if(getPermission() != null && !map.getPermittedPredicate().test(executor, getPermission())) {
            throw new NotPermittedException(getPermission());
        }

        // Execute without parsing because of no arguments and no parameters (Performance)
        if (this.parameters.isEmpty() && arguments.empty()) {
            this.executionContextConsumer.accept(new ExecutionContext<>(executor));
            return true;
        }

        // Invalid Syntax, because of empty arguments
        if (!this.parameters.isEmpty() && arguments.empty()) {
            throw new SyntaxException(getUsage());
        }

        Set<Argument> providedArguments = new HashSet<>();

        // Parsing
        try {
            parseFlagsAndArguments(executor, arguments, providedArguments);
        } catch (MissingArgumentException | MissingFlagArgument e) {
            throw new SyntaxException(getUsage());
        }

        System.out.println("Argument: " + providedArguments);

        ParserProvider parsers = this.map.getParserProvider();
        // Add default values
        this.parameters.stream()
                .filter(parameter -> providedArguments.stream().noneMatch(argument -> argument.getParameter() == parameter))
                .filter(parameter -> parameter.getDefaultValue() != null)
                .forEach(parameter -> {
                    providedArguments.add(parameter.newArgument(
                            parsers.parse(executor, ArgumentStack.createInvert(parameter.getDefaultValue().split(CommandMap.ARGUMENT_SEPARATOR)), parameter.getType(), parameter)
                    ));
                });
        this.flags.stream()
                .filter(parameter -> providedArguments.stream().noneMatch(argument -> argument.getParameter() == parameter))
                .filter(parameter -> parameter.getDefaultValue() != null)
                .forEach(parameter -> {
                    providedArguments.add(parameter.newArgument(
                            parsers.parse(executor, ArgumentStack.createInvert(parameter.getDefaultValue().split(CommandMap.ARGUMENT_SEPARATOR)), parameter.getType(), parameter)
                    ));
                });

        System.out.println("End arguments: " + providedArguments);

        this.executionContextConsumer.accept(new ExecutionContext<>(executor, providedArguments.toArray(new Argument[0])));
        return true;
    }

    @Override
    public @NotNull Collection<String> getSuggestions(E executor, ArgumentStack arguments, String written) {

        if (arguments.empty() && this.parameters.isEmpty() && this.flags.isEmpty())
            return Collections.emptyList();

        Set<Argument> list = new HashSet<>();

        ParserProvider parsers = this.map.getParserProvider();
        try {
//            if (!written.isEmpty()) {
//                arguments.insertElementAt(written, 0);
//            }
            parseFlagsAndArguments(executor, arguments, list);
//            return getSuggestionsStrings(arguments, written);
        } catch (MissingFlagArgument e) {
            System.err.println("Missing flag argument");
            FlagCommandParameter parameter = e.getFlagParameter();
            Parser<?> parser = parsers.getParser(parameter.getType());
            if (parser == null) {
                throw new MissingParserException();
            }
            return parser.getSuggestionsStrings(executor, arguments, written, parameter);
        } catch (MissingArgumentException e) {
            System.err.println("Missing argument");

            if (this.parameters.isEmpty() && !this.flags.isEmpty() && written.isEmpty() && list.isEmpty()) {
                return this.flags.stream()
                        .map(parameter -> "-" + parameter.getFlagChar())
                        .collect(Collectors.toList());
            }

            if (!this.flags.isEmpty() && written.startsWith("-")) {
                List<String> flagCharacters = Arrays.asList(written.substring(1).split(""));
                System.out.println("Suggest flags");
                System.out.println(flagCharacters);
                return this.flags.stream()
                        .filter(flagParameter -> flagCharacters.stream().noneMatch(argument ->
                                String.valueOf(flagParameter.getFlagChar()).equals(argument))
                                && list.stream()
                                .map(Argument::getParameter)
                                .filter(parameter -> parameter instanceof FlagCommandParameter)
                                .map(parameter -> (FlagCommandParameter) parameter)
                                .noneMatch(parameter -> {
                                    System.out.println(parameter.getFlagChar() + ": " + flagParameter.getFlagChar());
                                    return parameter.getFlagChar() == flagParameter.getFlagChar();
                                }))
                        .map(parameter -> written + parameter.getFlagChar())
                        .collect(Collectors.toList());
            }

            System.err.println("Missing argument");
            CommandParameter parameter = e.getParameter();
            Parser<?> parser = parsers.getParser(parameter.getType());
            if (parser == null) {
                throw new MissingParserException();
            }
            System.out.println(parser);
            return parser.getSuggestionsStrings(executor, arguments, written, parameter);
//            int skip = 0;
//            for (Parameter parameter : this.parameters) {
//                Parser<?> parser = parsers.getParser(parameter.getType());
//                if (parser == null) {
//                    throw new MissingParserException();
//                }
//                if (arguments.size() >= parser.getParameters(parameter).size() + skip) {
//                    skip += parser.getParameters(parameter).size();
//                    continue;
//                }
//                return parser.getSuggestionsStrings(arguments, written, parameter);
//            }
        } catch (ParseException e) {
            System.err.println("Parse Error");
            return e.getParser().getSuggestionsStrings(executor, arguments, written, e.getParameter());
        } catch (EmptyFlagException e) {
            return this.flags.stream()
                    .filter(flagParameter -> list.stream().noneMatch(argument -> argument.getParameter().getName().equals(flagParameter.getName())))
                    .map(parameter -> written.trim() + parameter.getFlagChar())
                    .collect(Collectors.toList());
        } catch (CommandException e) {
            System.out.println(e.getClass());
            return Collections.emptyList();
        }
//
        System.out.println("GOT");

        if (!this.flags.isEmpty() && written.startsWith("-")) {
            List<String> flagCharacters = Arrays.asList(written.substring(1).split(""));
            System.out.println("Suggest flags");
            System.out.println(flagCharacters);
            return this.flags.stream()
                    .filter(flagParameter -> flagCharacters.stream().noneMatch(argument ->
                            String.valueOf(flagParameter.getFlagChar()).equals(argument))
                            && list.stream()
                            .map(Argument::getParameter)
                            .filter(parameter -> parameter instanceof FlagCommandParameter)
                            .map(parameter -> (FlagCommandParameter) parameter)
                            .noneMatch(parameter -> {
                                System.out.println(parameter.getFlagChar() + ": " + flagParameter.getFlagChar());
                                return parameter.getFlagChar() == flagParameter.getFlagChar();
                            }))
                    .map(parameter -> written + parameter.getFlagChar())
                    .collect(Collectors.toList());
        }

//
//
        int skip = 0;

//        for (Parameter parameter : this.parameters) {
//            Parser<?> parser = parsers.getParser(parameter.getType());
//            if (parser == null) {
//                throw new MissingParserException();
//            }
//            if (arguments.size() >= parser.getParameters(parameter).size() + skip) {
//                skip += parser.getParameters(parameter).size();
//                continue;
//            }
//            return parser.getSuggestionsStrings(arguments, written, parameter);
//        }
//        if (arguments.empty()) {
//            Parameter parameter = this.arguments.get(0);
//            return parser.getSuggestionsStrings(args, written, parameter);
//        }
//        System.out.println("COMMAND SUGGESTION!!!");
        return Collections.emptyList();
    }

    @Deprecated
    private Collection<String> getSuggestionsStrings(E executor, ArgumentStack args, String written) {
        ParserProvider parsers = this.map.getParserProvider();
        Iterator<CommandParameter> parameters = this.parameters.iterator();

        Set<Argument> arguments = new HashSet<>();
        try {
            parseFlagsAndArguments(executor, args, arguments);
        } catch (MissingFlagArgument e) {
            return parsers.getParser(e.getFlagParameter().getType()).getSuggestionsStrings(executor, args, written, e.getFlagParameter());
        } catch (Exception e) {}

        while (!args.empty()) {
            String argument = args.pop();
            if (argument.trim().isEmpty())
                return Collections.emptyList();
        }

        System.out.println("Arguments: " + arguments);
        System.out.println("Written: " + written);
        if (written.startsWith("-")) {
            char[] flags = written.substring(1).toCharArray();

            if (flags.length == 0) {
                return this.flags.stream()
                        .filter(flagParameter -> arguments.stream().noneMatch(argument -> argument.getParameter().equals(flagParameter)))
                        .map(parameter -> written + parameter.getFlagChar())
                        .collect(Collectors.toList());
            }

            for (char flag : flags) {

            }
        }

//        if(written.startsWith("-")) {
//            String flags = written.substring(1);
//            for (char c : flags.toCharArray()) {
//                if(c == ' ')
//                    return Collections.emptyList();
//
//            }
//        }

        return Collections.emptyList();
    }

    private void parseFlagsAndArguments(E executor, ArgumentStack args, Set<Argument> arguments) {
        ParserProvider parsers = this.map.getParserProvider();
        Iterator<CommandParameter> parameters = this.parameters.iterator();

        while (!args.empty()) {
            String argument = args.peek();
            System.out.println("Arg: " + argument);
            if (argument.isEmpty())
                throw new SyntaxException();

            if (argument.startsWith("-")) {
                String flagArg = args.pop().substring(1);

                char[] chars = flagArg.toCharArray();
                if (chars.length == 0) {
                    throw new EmptyFlagException();
                }
                for (int i = 0; i < chars.length; i++) {
                    char flagChar = chars[i];
                    FlagCommandParameter flagParameter = this.flags.stream()
                            .filter(p -> p.getFlagChar() == flagChar)
                            .findFirst()
                            .orElse(null);

                    if (flagParameter == null) {
                        throw new InvalidFlagException(argument);
                    }

                    // Toggle boolean flag
                    if (flagParameter.isSwitch()) {
                        String def = flagParameter.getDefaultValue();
                        boolean value = false;
                        if (def != null) {
                            Object parse = parsers.parse(executor, ArgumentStack.create(def),
                                    flagParameter.getType(),
                                    flagParameter);
                            value = (boolean) parse;
                        }
                        value = !value;

                        if (!arguments.add(flagParameter.newArgument(value))) {
                            throw new DuplicateFlagException(flagParameter);
                        }
                        continue;
                    }

                    if (args.empty()) {
                        throw new MissingFlagArgument(flagParameter);
                    }

                    if (i != chars.length - 1) {
                        throw new InvalidFlagSyntaxException();
                    }

                    if (args.size() < parsers.getParserParametersSize(flagParameter.getType(), flagParameter)) {
                        throw new MissingFlagArgument(flagParameter);
                    }

                    try {
                        Object value = parsers.parse(executor, args, flagParameter.getType(), flagParameter);
                        if (!arguments.add(flagParameter.newArgument(value))) {
                            throw new DuplicateFlagException(flagParameter);
                        }
                    } catch (ParseException e) {
                        throw new MissingArgumentException(flagParameter);
                    }

                }
                continue;
            }

            if (!parameters.hasNext()) {
                throw new ToManyArgumentsException(this.parameters.size(), args.toArray(new String[0]));
            }

            CommandParameter parameter = parameters.next();
//            try {
            Object value = parsers.parse(executor, args, parameter.getType(), parameter);
            arguments.add(parameter.newArgument(value));
//            } catch (ParseException e) {
//                throw new MissingArgumentException(parameter);
//            }

        }

        while (parameters.hasNext()) {
            CommandParameter next = parameters.next();
            if (next.getDefaultValue() == null)
                throw new MissingArgumentException(next);
        }
    }


    /**
     * Generates the command usage
     *
     * @return the command usage, which generated.
     */
    private String generateCommandUsage() {
        UsageMapper mapper = map.getUsageMapper();

        String[] switchFlagUsages = this.flags.stream()
                .filter(FlagCommandParameter::isSwitch)
                .map(FlagCommandParameter::getFlagChar)
                .map(String::valueOf)
                .toArray(String[]::new);

        String[] otherFlagUsages = this.flags.stream()
                .filter(flagArgument -> !flagArgument.isSwitch())
                .map(flagArgument -> "-" + flagArgument.getFlagChar() + " " + mapper.map(this.map.getParserProvider(), flagArgument))
                .toArray(String[]::new);

        String switchFlagUsage = (switchFlagUsages.length > 0 ? "-" + String.join("", switchFlagUsages) : "");
        String otherFlagUsage = (otherFlagUsages.length > 0 ? String.join(", ", otherFlagUsages) : "");

        String flagUsage = "";

        if (!switchFlagUsage.isEmpty() && otherFlagUsage.isEmpty()) {
            flagUsage = switchFlagUsage;
        } else if (switchFlagUsage.isEmpty() && !otherFlagUsage.isEmpty()) {
            flagUsage = otherFlagUsage;
        } else if (!switchFlagUsage.isEmpty()) {

            flagUsage = String.join(" ", switchFlagUsage, otherFlagUsage);
        }

        if (!flagUsage.isEmpty()) {
            flagUsage = "[" + flagUsage + "] ";
        }

        return flagUsage + this.parameters.stream()
                .map(parameter -> mapper.map(this.map.getParserProvider(), parameter))
                .collect(Collectors.joining(" "));
    }

    @Override
    public void close() throws IOException {

    }
}
