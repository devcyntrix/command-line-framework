package org.helixdev.command;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Stack;

@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class ArgumentStack extends Stack<String> {

    public static ArgumentStack create(String... items) {
        ArgumentStack stack = new ArgumentStack();
        for (String item : items) {
            stack.push(item);
        }
        return stack;
    }

    public static ArgumentStack createInvert(String... items) {
        ArgumentStack stack = new ArgumentStack();
        for (int i = items.length - 1; i >= 0; i--) {
            stack.push(items[i]);
        }
        return stack;
    }

}
