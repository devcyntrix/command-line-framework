package org.helixdev.command.parser.defaults;

import org.helixdev.command.ArgumentStack;
import org.helixdev.command.exception.OutOfRangeException;
import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parameter.attribute.RangeAttribute;
import org.helixdev.command.parser.Parser;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Collections;

public class NumberParser implements Parser<Number> {
    @Override
    public <E> @NotNull Number parse(E executor, ArgumentStack input, Class<?> parseTo, CommandParameter parameter) {
        BigDecimal value = new BigDecimal(input.pop());

        // Provide range annotation
        RangeAttribute range = null;
        if (parameter.hasAttribute(RangeAttribute.class))
            range = parameter.getAttribute(RangeAttribute.class);

        if (parseTo == Double.class || parseTo == Double.TYPE) {
            double v = value.doubleValue();
            if (range != null && !(v >= range.getFrom() && v <= range.getTo()))
                throw new OutOfRangeException(range);
            return v;
        } else if (parseTo == Float.class || parseTo == Float.TYPE) {
            float v = value.floatValue();
            if (range != null && !(v >= range.getFrom() && v <= range.getTo()))
                throw new OutOfRangeException(range);
            return v;
        } else if (parseTo == Long.class || parseTo == Long.TYPE) {
            long v = value.longValue();
            if (range != null && !(v >= range.getFrom() && v <= range.getTo()))
                throw new OutOfRangeException(range);
            return v;
        } else if (parseTo == Integer.class || parseTo == Integer.TYPE) {
            int v = value.intValue();
            if (range != null && !(v >= range.getFrom() && v <= range.getTo()))
                throw new OutOfRangeException(range);
            return v;
        } else if (parseTo == Short.class || parseTo == Short.TYPE) {
            short v = value.shortValue();
            if (range != null && !(v >= range.getFrom() && v <= range.getTo()))
                throw new OutOfRangeException(range);
            return v;
        } else if (parseTo == Byte.class || parseTo == Byte.TYPE) {
            byte v = value.byteValue();
            if (range != null && !(v >= range.getFrom() && v <= range.getTo()))
                throw new OutOfRangeException(range);
            return v;
        } else if (parseTo == BigInteger.class) {
            return value.toBigInteger();
        }

        return value;
    }

    @Override
    public <E> Collection<String> getSuggestionsStrings(E executor, ArgumentStack args, String written, CommandParameter parameter) {
        return Collections.emptyList();
    }
}
