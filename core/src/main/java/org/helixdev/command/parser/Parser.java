package org.helixdev.command.parser;

import org.helixdev.command.ArgumentStack;
import org.helixdev.command.parameter.CommandParameter;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Collections;

public interface Parser<T> {

    <E> @NotNull T parse(E executor, ArgumentStack stack, Class<?> parseTo, CommandParameter parameter) throws Exception;

    default Collection<CommandParameter> getParameters(CommandParameter parameter) {
        return Collections.singleton(parameter);
    }

    <E> Collection<String> getSuggestionsStrings(E executor, ArgumentStack stack, String written, CommandParameter parameter);

}
