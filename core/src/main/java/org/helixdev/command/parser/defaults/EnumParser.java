package org.helixdev.command.parser.defaults;

import org.helixdev.command.ArgumentStack;
import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parser.Parser;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

public class EnumParser implements Parser<Enum<?>> {

    @Override
    public <E> @NotNull Enum<?> parse(E executor, ArgumentStack stack, Class<?> parseTo, CommandParameter parameter) throws Exception {
        return Enum.valueOf((Class<Enum>) parseTo, stack.pop().toUpperCase());
    }

    @Override
    public <E> Collection<String> getSuggestionsStrings(E executor, ArgumentStack stack, String written, CommandParameter parameter) {
        try {
            Method method = parameter.getType().getMethod("name");
            return Arrays.stream(parameter.getType().getEnumConstants())
                    .map(o -> {
                        try {
                            return method.invoke(o);
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                        return null;
                    })
                    .filter(Objects::nonNull)
                    .map(Object::toString)
                    .collect(Collectors.toList());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return Arrays.stream(parameter.getType().getEnumConstants())
                .map(Object::toString)
                .collect(Collectors.toList());
    }
}
