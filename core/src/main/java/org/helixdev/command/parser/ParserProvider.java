package org.helixdev.command.parser;

import lombok.Getter;
import org.helixdev.command.ArgumentStack;
import org.helixdev.command.exception.CommandException;
import org.helixdev.command.exception.MissingParserException;
import org.helixdev.command.exception.ParseException;
import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parser.defaults.BooleanParser;
import org.helixdev.command.parser.defaults.EnumParser;
import org.helixdev.command.parser.defaults.NumberParser;
import org.helixdev.command.parser.defaults.StringParser;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ParserProvider {

    private static final EnumParser ENUM_PARSER = new EnumParser();

    @Getter
    private final Map<Type, Parser<?>> adapterMap = new ConcurrentHashMap<>();

    public ParserProvider() {
        register(new BooleanParser(), Boolean.class, Boolean.TYPE);
        register(new NumberParser(),
                Double.class, Double.TYPE,
                Float.class, Float.TYPE,
                Long.class, Long.TYPE,
                Integer.class, Integer.TYPE,
                Short.class, Short.TYPE,
                Byte.class, Byte.TYPE);
        register(new StringParser(), String.class);
    }

    public void register(Parser<?> parser, Type type, Type... classes) {
        this.adapterMap.put(type, parser);
        for (Type aClass : classes) {
            this.adapterMap.put(aClass, parser);
        }
    }

    public @Nullable Parser<?> getParser(Class<?> parsingClass) {
        if (parsingClass.isEnum()) {
            return ENUM_PARSER;
        }
        return this.adapterMap.get(parsingClass);
    }

    public int getParserParametersSize(Class<?> parsingClass, CommandParameter parameter) {
        Parser<?> parser = getParser(parsingClass);
        if (parser == null)
            throw new MissingParserException();
        return parser.getParameters(parameter).size();
    }

    public <E> @NotNull Object parse(@NotNull E executor, @NotNull ArgumentStack input, @NotNull Class<?> parsingClass, @NotNull CommandParameter parameter) throws ParseException {
        Parser<?> parser = getParser(parsingClass);
        if (parser == null)
            throw new MissingParserException();
        try {
            return parser.parse(executor, input, parsingClass, parameter);
        } catch (CommandException e) {
            throw e;
        } catch (Exception e) {
            throw new ParseException(e, parser, parameter);
        }
    }

    public void clear() {
        this.adapterMap.clear();
    }

}
