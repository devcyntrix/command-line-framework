package org.helixdev.command.parser.defaults;

import org.helixdev.command.ArgumentStack;
import org.helixdev.command.exception.MissingQuoteEndException;
import org.helixdev.command.exception.NoMatchException;
import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parameter.attribute.string.GreedyAttribute;
import org.helixdev.command.parameter.attribute.string.MatchesAttribute;
import org.helixdev.command.parameter.attribute.string.QuotedAttribute;
import org.helixdev.command.parser.Parser;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

public class StringParser implements Parser<String> {

    @Override
    public <E> @NotNull String parse(E executor, ArgumentStack input, Class<?> parseTo, CommandParameter parameter) {
        if (parameter.hasAttribute(MatchesAttribute.class)) {
            MatchesAttribute attribute = parameter.getAttribute(MatchesAttribute.class);
            String regexp = attribute.getRegexp();
            if (!input.peek().matches(regexp))
                throw new NoMatchException(attribute);
            return input.pop();
        }

        if (parameter.hasAttribute(GreedyAttribute.class)) {
            LinkedList<String> list = new LinkedList<>();
            while (!input.empty()) {
                list.add(input.pop());
            }
            return String.join(" ", list.toArray(new String[0]));
        }

        if (parameter.hasAttribute(QuotedAttribute.class)) {
            LinkedList<String> list = new LinkedList<>();
            list.add(input.pop());

            if (list.getLast().startsWith("\"")) {
                while (!input.empty()) {
                    list.add(input.pop());
                    if (list.getLast().endsWith("\""))
                        break;
                }
                if (!list.getLast().endsWith("\"")) {
                    throw new MissingQuoteEndException();
                }
                String output = String.join(" ", list.toArray(new String[0]));
                return output.substring(1, output.length() - 1);
            }
            return list.getLast(); // Quote annotation without quotes
        }
        return input.pop();
    }

    @Override
    public <E> Collection<String> getSuggestionsStrings(E executor, ArgumentStack args, String written, CommandParameter parameter) {
        return Collections.emptyList();
    }
}
