package org.helixdev.command.parser.defaults;

import org.helixdev.command.ArgumentStack;
import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parser.Parser;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collection;

public class BooleanParser implements Parser<Boolean> {

    @Override
    public @NotNull <E> Boolean parse(E executor, ArgumentStack stack, Class<?> parseTo, CommandParameter parameter) throws Exception {
        return Boolean.parseBoolean(stack.pop());
    }

    @Override
    public <E> Collection<String> getSuggestionsStrings(E executor, ArgumentStack stack, String written, CommandParameter parameter) {
        return Arrays.asList("true", "false");
    }
}
