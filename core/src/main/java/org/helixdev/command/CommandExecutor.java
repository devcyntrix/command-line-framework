package org.helixdev.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;

import java.io.Closeable;
import java.util.Collection;
import java.util.List;

public interface CommandExecutor<E> extends Closeable {

    boolean execute(E executor, ArgumentStack arguments);

    @NotNull Collection<String> getSuggestions(E executor, ArgumentStack arguments, String written);

    @Nullable String getPermission();

    @Nullable String getDescription();

    @Unmodifiable @NotNull List<String> getAliases();

}
