package org.helixdev.command.annotaion.string;

import org.helixdev.command.annotaion.Arg;
import org.helixdev.command.annotaion.Flag;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Can used with an {@link Arg} and a {@link Flag}
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Quoted {
}
