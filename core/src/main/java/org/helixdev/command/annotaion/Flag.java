package org.helixdev.command.annotaion;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Flag {

    char value();

    /**
     * This value is only for none boolean flags
     * @return the name of the value
     */
    String name() default "";

    /**
     * The default value
     * @return the default value
     */
    String def() default "";

    String description() default "";

}
