package org.helixdev.command.annotaion;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines the arguments of a command. Optional arguments can only be at the end of the command arguments.
 * <p>
 * The default value makes the argument optional if it is set.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Arg {

    String value();

    String description() default "";

    /**
     * If this value has been set, the argument is optional
     *
     * @return the default value which will parse to type of argument.
     */
    String def() default "";

}
