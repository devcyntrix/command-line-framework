package org.helixdev.bukkit.command.parser;

import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.helixdev.bukkit.command.exceptions.RegexParsingException;
import org.helixdev.command.ArgumentStack;
import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parser.Parser;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;

public class MultiPlayerParser implements Parser<Player[]> {
    @Override
    public <E> @NotNull Player @NotNull [] parse(E executor, ArgumentStack stack, Class<?> parseTo, CommandParameter parameter) throws Exception {
        String name = stack.pop();

        try {
            return Bukkit.getOnlinePlayers().stream().filter(player -> player.getName().matches(name)).toArray(Player[]::new);
        } catch (PatternSyntaxException exception) {
            throw new RegexParsingException(name);
        }
    }

    @Override
    public <E> Collection<String> getSuggestionsStrings(E executor, ArgumentStack stack, String written, CommandParameter parameter) {
        List<String> collect = Bukkit.getOnlinePlayers().stream().filter(player -> player.getName().toLowerCase().startsWith(written.toLowerCase()) || player.getName().toLowerCase().endsWith(written.toLowerCase()))
                .map(HumanEntity::getName)
                .collect(Collectors.toList());
        collect.add(".*");
        return collect;
    }
}
