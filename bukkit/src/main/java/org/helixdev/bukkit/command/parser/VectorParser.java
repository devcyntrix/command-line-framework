package org.helixdev.bukkit.command.parser;

import org.bukkit.util.Vector;
import org.helixdev.command.ArgumentStack;
import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parser.Parser;
import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.*;

public class VectorParser implements Parser<Vector> {

    private final DecimalFormat format = new DecimalFormat("0.00", DecimalFormatSymbols.getInstance());

    @Override
    public <E> @NotNull Vector parse(E executor, ArgumentStack stack, Class<?> parseTo, CommandParameter parameter) throws Exception {
        double x = format.parse(stack.pop()).doubleValue();
        double y = format.parse(stack.pop()).doubleValue();
        double z = format.parse(stack.pop()).doubleValue();
        return new Vector(x, y, z);
    }

    @Override
    public Collection<CommandParameter> getParameters(CommandParameter parameter) {
        return Arrays.asList(
                new CommandParameter("x", double.class),
                new CommandParameter("y", double.class),
                new CommandParameter("z", double.class)
        );
    }


    @Override
    public <E> Collection<String> getSuggestionsStrings(E executor, ArgumentStack stack, String written, CommandParameter parameter) {
        List<String> suggestions = new LinkedList<>();

        if (!written.isEmpty()) {
            try {
                format.parse(written);
            } catch (ParseException e) {
                return Collections.emptyList();
            }
        }

        for (int i = 0; i < 10; i++) {
            suggestions.add(written + i);
        }
        return suggestions;
    }
}
