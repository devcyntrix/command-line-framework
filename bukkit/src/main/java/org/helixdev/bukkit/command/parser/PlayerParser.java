package org.helixdev.bukkit.command.parser;

import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.helixdev.bukkit.command.exceptions.PlayerNotFoundException;
import org.helixdev.command.ArgumentStack;
import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parser.Parser;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PlayerParser implements Parser<Player> {

    @Override
    public <E> @NotNull Player parse(E executor, ArgumentStack stack, Class<?> parseTo, CommandParameter parameter) {
        String name = stack.pop();

        if (name.equals("@r")) {
            Collection<? extends Player> players = Bukkit.getOnlinePlayers();
            Optional<? extends Player> first = players.stream()
                    .skip((int) (players.size() * Math.random()))
                    .findFirst();
            if (!first.isPresent())
                throw new PlayerNotFoundException("Random");
            return first.get();
        }

        Player player = Bukkit.getPlayer(name);
        if (player == null)
            throw new PlayerNotFoundException(name);
        return player;
    }

    @Override
    public <E> Collection<String> getSuggestionsStrings(E executor, ArgumentStack args, String written, CommandParameter parameter) {
        List<String> collect = Bukkit.getOnlinePlayers().stream().filter(player -> player.getName().toLowerCase().startsWith(written.toLowerCase()) || player.getName().toLowerCase().endsWith(written.toLowerCase()))
                .map(HumanEntity::getName)
                .collect(Collectors.toList());
        collect.add("@r");
        return collect;
    }
}
