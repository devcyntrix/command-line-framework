package org.helixdev.bukkit.command.parser;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.helixdev.bukkit.command.exceptions.WorldNotFoundException;
import org.helixdev.command.ArgumentStack;
import org.helixdev.command.parameter.CommandParameter;
import org.helixdev.command.parser.Parser;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.stream.Collectors;

public class WorldParser implements Parser<World> {

    @Override
    public <E> @NotNull World parse(E executor, ArgumentStack stack, Class<?> parseTo, CommandParameter parameter) throws Exception {
        String name = stack.pop();
        World world = Bukkit.getWorld(name);
        if (world == null)
            throw new WorldNotFoundException(name);
        return world;
    }

    @Override
    public <E> Collection<String> getSuggestionsStrings(E executor, ArgumentStack args, String written, CommandParameter parameter) {
        return Bukkit.getWorlds().stream().filter(world -> world.getName().startsWith(written) || world.getName().endsWith(written))
                .map(World::getName)
                .collect(Collectors.toList());
    }
}
