package org.helixdev.bukkit.command;

import lombok.Getter;
import me.lucko.commodore.CommodoreProvider;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.StringUtil;
import org.bukkit.util.Vector;
import org.helixdev.bukkit.command.exceptions.RegexParsingException;
import org.helixdev.bukkit.command.parser.MultiPlayerParser;
import org.helixdev.bukkit.command.parser.PlayerParser;
import org.helixdev.bukkit.command.parser.VectorParser;
import org.helixdev.bukkit.command.parser.WorldParser;
import org.helixdev.command.ArgumentStack;
import org.helixdev.command.CommandExecutor;
import org.helixdev.command.CommandMap;
import org.helixdev.command.exception.CommandException;
import org.helixdev.command.exception.InvalidFlagException;
import org.helixdev.command.exception.NotPermittedException;
import org.helixdev.command.exception.SyntaxException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class PluginCommandMap extends CommandMap<CommandSender> {

    @Getter
    private final Plugin plugin;

    @Getter
    private org.bukkit.command.CommandMap bukkitCommandMap;

    protected PluginCommandMap(@Nullable CommandMap<CommandSender> parent, @NotNull Plugin plugin) {
        super(parent);
        this.plugin = plugin;
        this.bukkitCommandMap = provideBukkitCommandMap();
        if (this.bukkitCommandMap == null) {
            throw new IllegalStateException("Failed to provide the bukkit command map");
        }
        super.setPermittedPredicate(Permissible::hasPermission);

        getParserProvider().register(new PlayerParser(), Player.class);
        getParserProvider().register(new MultiPlayerParser(), Player[].class);
        getParserProvider().register(new WorldParser(), World.class);
        getParserProvider().register(new VectorParser(), Vector.class);

        plugin.getLogger().info("Commodore supported: " + CommodoreProvider.isSupported());

        getErrorHandlerRegistry().register(SyntaxException.class, (executor, exception) -> executor.sendMessage("§c/" + exception.getMessage()));
        getErrorHandlerRegistry().register(InvalidFlagException.class, (executor, exception) -> executor.sendMessage("§cInvalid flag: " + ((InvalidFlagException) exception).getFlag()));
        getErrorHandlerRegistry().register(RegexParsingException.class, (executor, exception) -> executor.sendMessage("§cInvalid regex: " + ((RegexParsingException) exception).getPattern()));
        getErrorHandlerRegistry().register(NotPermittedException.class, (executor, e) -> executor.sendMessage("§cNot permitted!"));
    }

    public @Nullable org.bukkit.command.CommandMap provideBukkitCommandMap() {
        Server server = Bukkit.getServer();
        Class<? extends Server> craftServerClass = server.getClass();
        try {
            // Try to get the field
            Field commandMap = craftServerClass.getDeclaredField("commandMap");
            commandMap.setAccessible(true);
            return (org.bukkit.command.CommandMap) commandMap.get(server);
        } catch (NoSuchFieldException | IllegalAccessException ignored) {
            try {
                Method getCommandMap = craftServerClass.getDeclaredMethod("getCommandMap");
                getCommandMap.setAccessible(true);
                return (org.bukkit.command.CommandMap) getCommandMap.invoke(server);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public CommandExecutor<CommandSender> registerExecutor(String command, CommandExecutor<CommandSender> executor) {

        CommandExecutor<CommandSender> success = super.registerExecutor(command, executor);
        if (success == null)
            return null;

        String name = command;

        if (name.contains(ARGUMENT_SEPARATOR))
            name = name.split(ARGUMENT_SEPARATOR)[0];

        PluginCommand pluginCommand = newPluginCommand(name);
        if (pluginCommand == null)
            return null;

        if (executor.getDescription() != null)
            pluginCommand.setDescription(executor.getDescription());
        if (executor.getAliases() != null)
            pluginCommand.setAliases(executor.getAliases());
        if (executor.getPermission() != null)
            pluginCommand.setPermission(executor.getPermission());

//
//        if (method.isAnnotationPresent(Requires.class)) {
//            Requires requires = method.getAnnotation(Requires.class);
//            pluginCommand.setPermission(requires.value());
//        }


        pluginCommand.setExecutor((sender, bukkitCommand, alias, args) -> {
            try {
                ArgumentStack stack = ArgumentStack.createInvert(args);
                stack.push(bukkitCommand.getName());

                return execute(sender, stack);
            } catch (CommandException e) {
                boolean handleSuccess = PluginCommandMap.this.handleError(sender, e);
                if (!handleSuccess) {
                    throw e;
                }
            }
            return true;
        });

        pluginCommand.setTabCompleter((sender, bukkitCommand, alias, args) -> {
            LinkedList<String> test = new LinkedList<>(Arrays.asList(args));
            String written = "";
            if (!test.isEmpty()) {
                written = test.removeLast();
            }
            test.addFirst(bukkitCommand.getName());
            sender.sendMessage(test.toString());
            Collection<String> suggestions = getSuggestions(sender, ArgumentStack.createInvert(test.toArray(new String[0])), written);

            List<String> completions = new LinkedList<>();
            StringUtil.copyPartialMatches(written, suggestions, completions);
            return completions;
        });


        if (!this.bukkitCommandMap.register(getPlugin().getName(), pluginCommand)) {
            return null;
        }


        return success;
    }

    private PluginCommand newPluginCommand(String name) {
        Class<PluginCommand> pluginCommandClass = PluginCommand.class;
        try {
            Constructor<PluginCommand> constructor = pluginCommandClass.getDeclaredConstructor(String.class, Plugin.class);
            constructor.setAccessible(true);
            return constructor.newInstance(name, getPlugin());
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ignored) {}
        return null;
    }

    public static @NotNull PluginCommandMap create(@NotNull Plugin plugin) {
        return new PluginCommandMap(null, plugin);
    }
}
