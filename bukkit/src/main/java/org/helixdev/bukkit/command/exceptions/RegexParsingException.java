package org.helixdev.bukkit.command.exceptions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.helixdev.command.exception.CommandException;

@Getter
@RequiredArgsConstructor
public class RegexParsingException extends CommandException {

    private final String pattern;

}
